from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'car_warehouse/home.html')

def contact(request):
    return render(request, 'car_warehouse/basic.html', {'content': ['If you would like to contact me, please email me at', 'justincastillo@gmail.com']})
