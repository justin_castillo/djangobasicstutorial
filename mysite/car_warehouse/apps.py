from django.apps import AppConfig


class CarWarehouseConfig(AppConfig):
    name = 'car_warehouse'
